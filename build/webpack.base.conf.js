'use strict'
const path = require('path')
const utils = require('./utils')
const config = require('../config')

module.exports = {
  context: path.resolve(__dirname, '../'),  
  entry: {
    app: "./src/index.tsx"
  },
  output: {
    path: config.build.assetsRoot,
    filename: '[name].js',
    publicPath: process.env.NODE_ENV === 'production'
      ? config.build.assetsPublicPath
      : config.dev.assetsPublicPath
  },  
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"]
  },
  module: {
    rules: [
      {
        test: /\.tsx$/,
        use: ["babel-loader", "awesome-typescript-loader"]
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      }
    ]
  },  
}