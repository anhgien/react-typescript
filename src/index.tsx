import App from './App';
import * as ReactDOM from "react-dom";
import * as React from "react";

const render = () => ReactDOM.render(
  <App />,
  document.getElementById('app')
)

render();